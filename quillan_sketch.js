(function($) {
  $(document).ready(function() {

    var sketchpad = Raphael.sketchpad('quillan_sketch_editor', {
      width: 400,
      height: 400,
      editing: true
    });

    saveSketch = function () {
      canvg('sketch_canvas', $("#quillan_sketch_editor").html());

      //~// the canvas calls to output a png
      var canvas = document.getElementById("sketch_canvas");
      var img = canvas.toDataURL("image/png");
      $.ajax({
        type: 'POST',
        url: Drupal.settings.savequillanurl,
        dataType: 'json',
        success: addSketch,
        data: 'img='+img
      });
    }

    addSketch = function (data) {
      $('#sketch_fid').val(data.fid);
      $("#quillan_sketch_editor").hide();
      $("#quillan_sketch_save").hide();
      $("#sketch_canvas").show();
      $("#quillan_sketch_saved").show();
      $('#edit-submit').removeAttr("disabled");
    }

    clearSketch = function () {
      sketchpad.json('[{"type":"path"}]');
    }

  });

})(jQuery);
