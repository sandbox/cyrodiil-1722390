<?php
/**
 * @file
 * sketch_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function sketch_feature_node_info() {
  $items = array(
    'quillan_sketch' => array(
      'name' => t('Quillan Sketch'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
